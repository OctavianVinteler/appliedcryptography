#!/usr/bin/env python

import argparse, datetime, hashlib, sys     # do not use any other imports/libraries
from smartcard.CardType import AnyCardType
from smartcard.CardRequest import CardRequest
from smartcard.CardConnection import CardConnection
from smartcard.util import toHexString, HexListToBinString
from pyasn1.codec.der import encoder

# specify here how much time your solution required
# time = 2 hours

####################################################### Asn1 Encoder ######################################################

def int_to_bytestring_asn1(i):
    s = ""
    while i:
        n = i & 0xff
        s = chr(n) + s
        i = i >> 8

    if s == "":
        s = chr(0x00)
    return s

def asn1_len(content):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    numberOfBytes = len(content)
    if numberOfBytes <= 127:
        return chr(numberOfBytes)
    else:
        numberOfLengthBytes = int_to_bytestring_asn1(numberOfBytes)
        return chr(128 | len(numberOfLengthBytes)) + numberOfLengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    typeRep = chr(0x02)
    rep = int_to_bytestring_asn1(i)

    temp = i
    while temp > 255:
        temp = temp >> 8

    if temp >= 128:
        rep = chr(0x00) + rep

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_enumerated(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of ENUMERATED
    typeRep = chr(0x0A)
    rep = int_to_bytestring_asn1(i)

    temp = i
    while temp > 255:
        temp = temp >> 8

    if temp >= 128:
        rep = chr(0x00) + rep

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    typeRep = chr(0x03)
    padding = 8 - (len(bitstr) % 8)

    if padding == 8:
        padding = 0

    bitstr = bitstr + (padding*"0")

    if bitstr == "":
        rep = ""
    else:
        rep = int_to_bytestring_asn1(int(bitstr, 2))

    length = asn1_len(chr(padding) + rep)

    return typeRep + length + chr(padding) + rep

def asn1_bistring_bytestring(intRepr):
    typeRep = chr(0x03)
    padding = 0

    length = asn1_len(chr(padding) + intRepr)

    return typeRep + length + chr(padding) + intRepr

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    typeRep = chr(0x04)
    length = asn1_len(octets)
    return typeRep + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    typeRep = chr(0x06)
    rep = ""

    if len(oid) >= 2:
        rep += chr(oid[0] * 40 + oid[1])
    elif len(oid) >= 1:
        rep += chr(oid[0] * 40)
    else:
        rep += chr(0x00)

    for index in range(2, len(oid)):
        number = oid[index]

        for x in range(10, 0, -1):
            power = pow(128,x)
            if number >= power:
                rep += chr(128 + (number / power))
                number = number - (number / power) * power

        rep += chr(oid[index] % 128)

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    typeRep = chr(0x30)
    length = asn1_len(der)

    return typeRep + length + der


def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    typeRep = chr(0x31)
    length = asn1_len(der)

    return typeRep + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    typeRep = chr(0x13)
    length = asn1_len(string)
    return typeRep + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    typeRep = chr(0x17)
    length = asn1_len(time)
    return typeRep + length + time

def asn1_generalizedtime(time):
    # returns DER encoding of UTCTime
    typeRep = chr(0x18)
    length = asn1_len(time)
    return typeRep + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    typeRep = chr(160 + tag)
    length = asn1_len(der)
    return typeRep + length + der

def asn1_tag_implicit(der, tag):
    formBit = ord(der[0]) & 32
    typeRep = chr(128 + formBit + tag)
    return typeRep + der[1:]

############################################################################################################################


# parse arguments
parser = argparse.ArgumentParser(description='Perform signing with ID card', add_help=False)
parser.add_argument("filetosign", type=str, help="File to sign")
parser.add_argument("signature", type=str, help="File to store signature")
parser.add_argument("--measure", default=None, action="store_true", help="Measure the time required to perform 100 operations")
args = parser.parse_args()

# this will wait for card inserted in any reader
channel = CardRequest(timeout=100, cardType=AnyCardType()).waitforcard().connection
print "[+] Selected reader:", channel.getReader()

# using T=0 for compatibility and simplicity
channel.connect(CardConnection.T0_protocol)

# detect and print EstEID card type (EstEID spec page 15)
atr = channel.getATR()
if atr == [0x3B,0xFE,0x94,0x00,0xFF,0x80,0xB1,0xFA,0x45,0x1F,0x03,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0x43]:
    print "[+] EstEID v1.0 on Micardo Public 2.1"
elif atr == [0x3B,0xDE,0x18,0xFF,0xC0,0x80,0xB1,0xFE,0x45,0x1F,0x03,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0x2B]:
    print "[+] EstEID v1.0 on Micardo Public 3.0 (2006)"
elif atr == [0x3B,0x6E,0x00,0x00,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30]:
    print "[+] EstEID v1.1 on MultiOS (DigiID)"
elif atr == [0x3B,0xFE,0x18,0x00,0x00,0x80,0x31,0xFE,0x45,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0xA8]:
    print "[+] EstEID v3.x on JavaCard"
elif atr == [0x3B,0xFA,0x18,0x00,0x00,0x80,0x31,0xFE,0x45,0xFE,0x65,0x49,0x44,0x20,0x2F,0x20,0x50,0x4B,0x49,0x03]:
    print "[+] EstEID v3.5 (10.2014) cold (eID)"
else:
    print "[-] Unknown card:", toHexString(atr)
    sys.exit()

def send(apdu):
    data, sw1, sw2 = channel.transmit(apdu)

    # success
    if [sw1,sw2] == [0x90,0x00]:
        return data
    # T=0 signals that there is more data to read
    elif sw1 == 0x61:
        return send([0x00, 0xC0, 0x00, 0x00, sw2]) # GET RESPONSE of sw2 bytes
    # probably error condition
    else:
        print "Error: %02x %02x, sending APDU: %s" % (sw1, sw2, toHexString(apdu))
        sys.exit()

print "[=] Signing file %s" % (args.filetosign)

send([0x00, 0xA4, 0x00, 0x0C])
send([0x00, 0xA4, 0x01, 0x0C, 0x02, 0xEE, 0xEE])

# Set the security environment
send([0x00, 0x22, 0xF3, 0x01])

# Compute the SHA1 digest
sha1 = hashlib.sha1()

fileReader = open(args.filetosign, 'rb')
data = fileReader.read(512)

while data:
    sha1.update(data)
    data = fileReader.read(512)

sha1digest = sha1.digest()

print "[+] Calculated SHA1 digest:", sha1digest.encode('hex')

# making asn1 hash object to sign according to PKCS#1 v1.5
print "[+] Making DigestInfo object..."
der = asn1_sequence(asn1_sequence(asn1_objectidentifier([1,3,14,3,2,26]) + asn1_null()) + asn1_octetstring(sha1digest))

def measure():
    pass


if args.measure:
    measure()
    sys.exit(0)

print "[?] Enter PIN2:",
pin2 = raw_input()

send([0x00, 0x20, 0x00, 0x02, len(pin2)] + [ord(c) for c in pin2])

# calculating the signature (EstEID spec page 40)
print "[+] Sending DigestInfo to smart card for signing..."
response = send([0x00, 0x2A, 0x9E, 0x9A, len(der)] + [ord(c) for c in der])

# save signature into file
print "[+] Signature saved into", args.signature
open(args.signature, 'w').write(HexListToBinString(response))
