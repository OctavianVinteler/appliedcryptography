#!/usr/bin/env python

import argparse, hashlib, os, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder

# specify here how much time your solution required
#time = 6

# parse arguments
parser = argparse.ArgumentParser(description='generate self-signed X.509 CA certificate', add_help=False)
parser.add_argument("private_key_file", help="Private key file (in PEM or DER form)")
parser.add_argument("output_cert_file", help="File to store self-signed CA certificate (PEM form)")
args = parser.parse_args()


####################################################### Asn1 Encoder ######################################################

def int_to_bytestring_asn1(i):
    s = ""
    while i:
        n = i & 0xff
        s = chr(n) + s
        i = i >> 8

    if s == "":
        s = chr(0x00)
    return s

def asn1_len(content):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    numberOfBytes = len(content)
    if numberOfBytes <= 127:
        #print "A: numberOfLengthBytes = " + str(numberOfBytes)
        return chr(numberOfBytes)
    else:
        numberOfLengthBytes = int_to_bytestring_asn1(numberOfBytes)
        return chr(128 | len(numberOfLengthBytes)) + numberOfLengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    typeRep = chr(0x02)
    rep = int_to_bytestring_asn1(i)

    temp = i
    while temp > 255:
        temp = temp >> 8

    if temp >= 128:
        rep = chr(0x00) + rep

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    typeRep = chr(0x03)
    padding = 8 - (len(bitstr) % 8)

    if padding == 8:
        padding = 0

    bitstr = bitstr + (padding*"0")

    if bitstr == "":
        rep = ""
    else:
        rep = int_to_bytestring_asn1(int(bitstr, 2))

    length = asn1_len(chr(padding) + rep)

    return typeRep + length + chr(padding) + rep

def asn1_bistring_bytestring(intRepr):
    typeRep = chr(0x03)
    padding = 0

    """bitstr = "";

    while intRepr > 0:
        byteRep = intRepr % 256
        intRepr = intRepr / 256
        while byteRep > 0:
            c = byteRep % 2
            bitstr = str(c) + bitstr
            byteRep = byteRep / 2

        lengthDiff = 8 - len(bitstr) % 8
        if lengthDiff == 8:
            lengthDiff = 0

        bitstr = lengthDiff*'0' + bitstr
    rep = int_to_bytestring_asn1(int(bitstr, 2))"""
    rep = int_to_bytestring_asn1(intRepr)

    length = asn1_len(chr(padding) + rep)

    return typeRep + length + chr(padding) + rep

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    typeRep = chr(0x04)
    length = asn1_len(octets)
    return typeRep + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    typeRep = chr(0x06)
    rep = ""

    if len(oid) >= 2:
        rep += chr(oid[0] * 40 + oid[1])
    elif len(oid) >= 1:
        rep += chr(oid[0] * 40)
    else:
        rep += chr(0x00)

    for index in range(2, len(oid)):
        number = oid[index]

        for x in range(10, 0, -1):
            power = pow(128,x)
            if number >= power:
                rep += chr(128 + (number / power))
                number = number - (number / power) * power

        rep += chr(oid[index] % 128)

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    typeRep = chr(0x30)
    length = asn1_len(der)

    return typeRep + length + der


def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    typeRep = chr(0x31)
    length = asn1_len(der)

    return typeRep + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    typeRep = chr(0x13)
    length = asn1_len(string)
    return typeRep + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    typeRep = chr(0x17)
    length = asn1_len(time)
    return typeRep + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    typeRep = chr(160 + tag)
    length = asn1_len(der)
    return typeRep + length + der

############################################################################################################################

def int_to_bytestring(i, length):
    # converts integer to bytestring
    s = ""
    for smth in xrange(length):
        s = chr(i & 0xff) + s
        i >>= 8
    return s

def bytestring_to_int(s):
    # converts bytestring to integer
    i = 0
    for char in s:
        i <<= 8
        i |= ord(char)
    return i

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER

    #private key
    if content.startswith("-----BEGIN RSA PRIVATE KEY-----"):
        content = content.replace("-----BEGIN RSA PRIVATE KEY-----", "")
        content = content.replace("-----END RSA PRIVATE KEY-----", "")

    content = content.decode("base64")

    return content

def get_pubkey(filename):
    # reads private key file and returns (n, e)
    fileReader = open(filename, 'r')
    privkeyRep = fileReader.read()
    fileReader.close()

    if privkeyRep.startswith("-----"):
        privkeyRep = pem_to_der(privkeyRep)

    privkey = decoder.decode(privkeyRep)

    return int(privkey[0][1]), int(privkey[0][2])

def get_privkey(filename):
    # reads private key file and returns (n, d)
    fileReader = open(filename, 'r')
    privkeyRep = fileReader.read()
    fileReader.close()

    if privkeyRep.startswith("-----"):
        privkeyRep = pem_to_der(privkeyRep)

    privkey = decoder.decode(privkeyRep)

    return int(privkey[0][1]), int(privkey[0][3])

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5

    # calculate byte size of modulus n
    k = 0
    while n:
        k = k + 1
        n = n >> 8

    textLength = len(plaintext) + 3
    paddingLength = k - textLength

    # plaintext must be at least 3 bytes smaller than modulus
    padding = '\xFF'*paddingLength

    # generate padding bytes
    padded_plaintext = '\x00' + '\x01' + padding + '\x00' + plaintext
    return padded_plaintext

def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    paddingLength = 0
    if plaintext[0] == '\x00':
        paddingLength = 1
    else:
        return plaintext

    while plaintext[paddingLength] != '\x00':
        paddingLength += 1

    paddingLength += 1

    plaintext = plaintext[paddingLength:]
    return plaintext


def digestinfo_der(m):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA1 digest of m
    sha1 = hashlib.sha1()

    sha1.update(m)

    der = asn1_sequence(asn1_sequence(asn1_objectidentifier([1,3,14,3,2,26]) + asn1_null()) + asn1_octetstring(sha1.digest()))
    return der

def sign(m, keyfile):
    # sign DigestInfo of message m
    n, d = get_privkey(keyfile)

    s = pow(m, d, n)

    k = 0
    while n:
        k = k + 1
        n = n >> 8

    c = int_to_bytestring(s, k)
    return c

def constructIssuer():
    country = 'EE'
    organizationName = 'Tartu Ulikool'
    commonName = 'Octavian Vinteler'
    organizationalUnitName = 'Tartu Ulikool Unit Name'

    der = asn1_sequence(asn1_set(asn1_sequence(asn1_objectidentifier([2,5,4,6]) + asn1_printablestring(country))) + asn1_set(asn1_sequence(asn1_objectidentifier([2,5,4,10]) + asn1_printablestring(organizationName))) + asn1_set(asn1_sequence(asn1_objectidentifier([2,5,4,11]) + asn1_printablestring(organizationalUnitName))) + asn1_set(asn1_sequence(asn1_objectidentifier([2,5,4,3]) + asn1_printablestring(commonName))))

    return der

def constructExtensions():
    encapsulation = asn1_sequence(asn1_boolean(True))
    return asn1_sequence(asn1_sequence(asn1_objectidentifier([2,5,29,15]) + asn1_octetstring(asn1_bitstring("0000011"))) + asn1_sequence(asn1_objectidentifier([2,5,29,19]) + asn1_boolean(True) + asn1_octetstring(encapsulation)))

def selfsigned(privkey, certfile):
    # create x509v3 self-signed CA root certificate
    serialNumber = 1111

    # get public key (n, e) from private key file
    n, e = get_pubkey(privkey)

    # construct subjectPublicKeyInfo from public key values (n, e)
    keyInfo = asn1_sequence(asn1_integer(n) + asn1_integer(e))
    #keyBits = bin(bytestring_to_int(keyInfo))
    keyBits = asn1_bistring_bytestring(bytestring_to_int(keyInfo))
    #subjectPublicKeyInfo = asn1_sequence(asn1_sequence(asn1_objectidentifier([1,2,840,113549,1,1,1]) + asn1_null()) + asn1_bitstring(keyBits))
    subjectPublicKeyInfo = asn1_sequence(asn1_sequence(asn1_objectidentifier([1,2,840,113549,1,1,1]) + asn1_null()) + keyBits)

    # construct tbsCertificate structure
    tbsCertificate = asn1_sequence(asn1_tag_explicit(asn1_integer(2), 0) + asn1_integer(serialNumber) + asn1_sequence(asn1_objectidentifier([1,2,840,113549,1,1,5]) + asn1_null()) + constructIssuer() + asn1_sequence(asn1_utctime("141223010900Z") + asn1_utctime("150730010900Z")) + constructIssuer() + subjectPublicKeyInfo + asn1_tag_explicit(constructExtensions(), 3))
    #print(str(decoder.decode(tbsCertificate)))

    # sign tbsCertificate structure
    paddedText = pkcsv15pad_sign(digestinfo_der(tbsCertificate), n)
    signature = sign(bytestring_to_int(paddedText), privkey)

    # construct final X.509 DER
    #cert = asn1_sequence(tbsCertificate + asn1_sequence(asn1_objectidentifier([1,2,840,113549,1,1,5]) + asn1_null()) + asn1_bitstring(bin(bytestring_to_int(signature))))
    cert = asn1_sequence(tbsCertificate + asn1_sequence(asn1_objectidentifier([1,2,840,113549,1,1,5]) + asn1_null()) + asn1_bistring_bytestring(bytestring_to_int(signature)))

    # convert to PEM by .encode('base64') and adding PEM headers
    pem = '-----BEGIN CERTIFICATE-----\n' + cert.encode('base64') + '-----END CERTIFICATE-----'

    # write PEM certificate to file
    open(certfile, 'w').write(pem)

selfsigned(args.private_key_file, args.output_cert_file)
