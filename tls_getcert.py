#!/usr/bin/env python

import argparse, datetime, os, socket, sys, time, urlparse # do not use any other imports/libraries

# specify here how much time your solution required
# time = 6 hours

# parse arguments
parser = argparse.ArgumentParser(description='TLS v1.0 client')
parser.add_argument('url', type=str, help='URL to request')
parser.add_argument('--certificate', type=str, help='File to write PEM-encoded server certificate')
args = parser.parse_args()

# converts bytes (big endian) to integer
def bn(bytes):
        num = 0
        for byte in bytes:
                num <<= 8
                num |= ord(byte)
        return num

# converts integer to bytes (big endian)
def nb(i, length=0):
    bytes = ""
    for smth in xrange(length):
        bytes = chr(i & 0xff) + bytes
        i >>= 8
    return bytes

# returns TLS record that contains client_hello handshake message
def client_hello():

    print "--> client_hello()"
    layer = "\x03\x01"
    layer += nb(int(time.time()), 4)
    layer += os.urandom(28)
    layer += nb(0, 1)

    # list of cipher suites the client supports
    csuite = "\x00\x05"	# TLS_RSA_WITH_RC4_128_SHA
    csuite+= "\x00\x2f" # TLS_RSA_WITH_AES_128_CBC_SHA
    csuite+= "\x00\x35" # TLS_RSA_WITH_AES_256_CBC_SHA

    layer += nb(len(csuite), 2)
    layer += csuite
    layer += nb(1, 1)
    layer += "\x00"

    # add handshake message header
    record = nb(1, 1) + nb(len(layer), 3) + layer

    # add record layer header
    record = "\x16" + "\x03\x01" + nb(len(record), 2) + record

    return record

# returns TLS record that contains 'Certificate unknown' fatal alert message
def alert():
    print "--> alert()"

    # add alert message
    record = "\x02" + "\x2E"

    # add record layer header
    record = "\x15" + "\x03\x01" + nb(len(record), 2) + record

    return record

# parse TLS handshake messages
def parsehandshake(r):
    global server_hello_done_received

    # read handshake message type and length from message header

    htype = r[0]

    bytesUsed = 0

    if htype == "\x02":
        print "	<--- server_hello()"

        dataLength = bn(r[1:4])

        tls = r[4:6]

        body = r[6:dataLength+6]

        bytesUsed = dataLength + 4

        server_random = body[0:32]
        gmt = datetime.datetime.fromtimestamp(bn(body[0:4])).strftime('%Y-%m-%d %H:%M:%S')

        sessidLength = bn(body[32])
        sessid = body[33:sessidLength+33]
        cipher = body[sessidLength+33:sessidLength+35]
        compression = body[sessidLength+35]

        print "	[+] server randomness:", server_random.encode('hex').upper()
        print "	[+] server timestamp:", gmt
        print "	[+] TLS session ID:", sessid.encode('hex').upper()

        if cipher=="\x00\x2f":
            print "	[+] Cipher suite: TLS_RSA_WITH_AES_128_CBC_SHA"
        elif cipher=="\x00\x35":
            print "	[+] Cipher suite: TLS_RSA_WITH_AES_256_CBC_SHA"
        elif cipher=="\x00\x39":
            print "	[+] Cipher suite: TLS_DHE_RSA_WITH_AES_256_CBC_SHA"
        elif cipher=="\x00\x05":
            print "	[+] Cipher suite: TLS_RSA_WITH_RC4_128_SHA"
        else:
            print "[-] Unsupported cipher suite selected:", cipher.encode('hex')
            sys.exit(1)

        if compression!="\x00":
            print "[-] Wrong compression:", compression.encode('hex')
            sys.exit(1)

    elif htype == "\x0b":
        print "	<--- certificate()"

        dataLength = bn(r[1:4])
        certificates = r[4:dataLength+4]

        bytesUsed = dataLength + 4

        certificatesLength = bn(certificates[0:3])
        certificatesData = certificates[3:certificatesLength+3]

        certlen = bn(certificatesData[0:3])

        print "	[+] Server certificate length:", certlen

        servCert = certificatesData[3:certlen+3]

        if args.certificate:
            open(args.certificate, 'w').write('-----BEGIN CERTIFICATE-----\n' + servCert.encode("base64") + '-----END CERTIFICATE-----')
            print "	[+] Server certificate saved in:", args.certificate

    elif htype == "\x0e":
        server_hello_done_received = True
        bytesUsed = 4
        print "	<--- server_hello_done()"
    else:
        print "[-] Unknown Handshake Type:", htype.encode('hex')
        sys.exit(1)

    # handle the case of several handshake messages in one record
    leftover =r[bytesUsed:]
    if len(leftover):
        parsehandshake(leftover)

# parses TLS record
def parserecord(r):

    # read from the TLS record header the content type and length
    header = r[0:5]
    dataLen = bn(header[3:5])

    if header[0] == "\x16":
        print "<--- handshake()"
        parsehandshake(r[5:dataLen + 5])



# read from the socket full TLS record
def readrecord():
    record = ""

    # read the TLS record header (5 bytes)
    header = ""

    while len(header) < 5:
        buff = s.recv(1)
        if not buff:
            print "[-] Connection Closed - Program exiting..."
            sys.exit(1)
        header = header + buff

    # find data length
    dataLen = bn(header[3:5])

    # read the TLS record body
    record = ""

    while len(record) < dataLen:
        buff = s.recv(1)
        if not buff:
            print "[-] Connection Closed - Program exiting..."
            sys.exit(1)
        record = record + buff

    record = header + record

    return record

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
url = urlparse.urlparse(args.url)
host = url.netloc.split(':')
if len(host) > 1:
    port = int(host[1])
else:
    port = 443
host = host[0]
path = url.path

s.connect((host, port))
s.send(client_hello())

server_hello_done_received = False
while not server_hello_done_received:
    parserecord(readrecord())
s.send(alert())

print "[+] Closing TCP connection!"
s.close()
