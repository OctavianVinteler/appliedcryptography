#!/usr/bin/env python

import argparse, sys     # do not use any other imports/libraries
from smartcard.CardType import AnyCardType
from smartcard.CardRequest import CardRequest
from smartcard.CardConnection import CardConnection
from smartcard.util import toHexString, HexListToBinString

# specify here how much time your solution required
# time = 4 hours

# parse arguments
parser = argparse.ArgumentParser(description='Fetch certificates from ID card', add_help=False)
parser.add_argument('--cert', type=str, default=None, choices=['auth','sign'], help='Which certificate to fetch')
parser.add_argument("--out", required=True, type=str, help="File to store certifcate (PEM)")
args = parser.parse_args()


# this will wait for card inserted in any reader
channel = CardRequest(timeout=100, cardType=AnyCardType()).waitforcard().connection
print "[+] Selected reader:", channel.getReader()

# using T=0 for compatibility and simplicity
channel.connect(CardConnection.T0_protocol)

# detect and print EstEID card type (EstEID spec page 14)
atr = channel.getATR()
if atr == [0x3B,0xFE,0x94,0x00,0xFF,0x80,0xB1,0xFA,0x45,0x1F,0x03,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0x43]:
    print "[+] EstEID v1.0 on Micardo Public 2.1"
elif atr == [0x3B,0xDE,0x18,0xFF,0xC0,0x80,0xB1,0xFE,0x45,0x1F,0x03,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0x2B]:
    print "[+] EstEID v1.0 on Micardo Public 3.0 (2006)"
elif atr == [0x3B,0x6E,0x00,0x00,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30]:
    print "[+] EstEID v1.1 on MultiOS (DigiID)"
elif atr == [0x3B,0xFE,0x18,0x00,0x00,0x80,0x31,0xFE,0x45,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0xA8]:
    print "[+] EstEID v3.x on JavaCard"
else:
    print "[-] Unknown card:", toHexString(atr)
    sys.exit()

def send(apdu):
    data, sw1, sw2 = channel.transmit(apdu)

    # success
    if [sw1,sw2] == [0x90,0x00]:
        return data
    # T=0 signals that there is more data to read
    elif sw1 == 0x61:
	print "[=] More data to read:", sw2
        return send([0x00, 0xC0, 0x00, 0x00, sw2]) # GET RESPONSE of sw2 bytes
    # probably error condition
    else:
        print "Error: %02x %02x, sending APDU: %s" % (sw1, sw2, toHexString(apdu))
        sys.exit()

# reading from card auth or sign certificate (EstEID spec page 33)
print "[=] Retrieving %s certificate..." % (args.cert)
if args.cert == 'auth':
    send([0x00, 0xA4, 0x00, 0x0C])
    send([0x00, 0xA4, 0x01, 0x0C, 0x02, 0xEE, 0xEE])
    send([0x00, 0xA4, 0x02, 0x0C, 0x02, 0xAA, 0xCE])
elif args.cert == 'sign':
    send([0x00, 0xA4, 0x00, 0x0C])
    send([0x00, 0xA4, 0x01, 0x0C, 0x02, 0xEE, 0xEE])
    send([0x00, 0xA4, 0x02, 0x0C, 0x02, 0xDD, 0xCE])

# read first 10 bytes to parse ASN.1 length field and determine certificate length
record = send([0x00, 0xB0, 0, 0, 10])

lengthByte = record[1]

if lengthByte <= 127:
    certlen = lengthByte
    bytesToAdd = 2
else:
    lengthByte = lengthByte & 127
    bytesToAdd = 2 + lengthByte

    certlen = 0
    for i in xrange(lengthByte, 0, -1):
        certlen = certlen * 256 + record[2 + lengthByte - i]

#Taking into account the type byte and the length bytes
certlen += bytesToAdd

print "[+] Certificate size: %d bytes" % (certlen)

# reading DER encoded certificate from smart card
bytesRead = 0
cert = ""
while certlen > 255:
    cert = cert + HexListToBinString(send([0x00, 0xB0, bytesRead/256, bytesRead%256, 255]))
    bytesRead += 255
    certlen = certlen - 255

cert = cert + HexListToBinString(send([0x00, 0xB0, bytesRead/256, bytesRead%256, certlen]))

# save certificate in PEM form
open(args.out,"wb").write("-----BEGIN CERTIFICATE-----\n"+cert.encode('base64')+"-----END CERTIFICATE-----\n")
print "[+] Certificate stored in", args.out
