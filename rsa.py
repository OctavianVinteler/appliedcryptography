#!/usr/bin/env python

import hashlib, os, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder

# specify here how much time your solution required
#time = 11 hours

####################################################### Asn1 Encoder ######################################################

def int_to_bytestring_asn1(i):
    s = ""
    while i:
        n = i & 0xff
        s = chr(n) + s
        i = i >> 8

    if s == "":
        s = chr(0x00)
    return s

def asn1_len(content):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    numberOfBytes = len(content)
    if numberOfBytes <= 127:
        #print "A: numberOfLengthBytes = " + str(numberOfBytes)
        return chr(numberOfBytes)
    else:
        numberOfLengthBytes = int_to_bytestring_asn1(numberOfBytes)
        return chr(128 | len(numberOfLengthBytes)) + numberOfLengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    typeRep = chr(0x02)
    rep = int_to_bytestring_asn1(i)

    temp = i
    while temp > 255:
        temp = temp >> 8

    if temp >= 128:
        rep = chr(0x00) + rep

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    typeRep = chr(0x03)
    padding = 8 - (len(bitstr) % 8)

    if padding == 8:
        padding = 0

    bitstr = bitstr + (padding*"0")

    if bitstr == "":
        rep = ""
    else:
        rep = int_to_bytestring_asn1(int(bitstr, 2))

    length = asn1_len(chr(padding) + rep)

    return typeRep + length + chr(padding) + rep

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    typeRep = chr(0x04)
    length = asn1_len(octets)
    return typeRep + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    typeRep = chr(0x06)
    rep = ""

    if len(oid) >= 2:
        rep += chr(oid[0] * 40 + oid[1])
    elif len(oid) >= 1:
        rep += chr(oid[0] * 40)
    else:
        rep += chr(0x00)

    for index in range(2, len(oid)):
        number = oid[index]

        for x in range(10, 0, -1):
            power = pow(128,x)
            if number >= power:
                rep += chr(128 + (number / power))
                number = number - (number / power) * power

        rep += chr(oid[index] % 128)

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    typeRep = chr(0x30)
    length = asn1_len(der)

    return typeRep + length + der


def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    typeRep = chr(0x31)
    length = asn1_len(der)

    return typeRep + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    typeRep = chr(0x13)
    length = asn1_len(string)
    return typeRep + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    typeRep = chr(0x17)
    length = asn1_len(time)
    return typeRep + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    typeRep = chr(160 + tag)
    length = asn1_len(der)
    return typeRep + length + der

############################################################################################################################

def int_to_bytestring(i, length):
    # converts integer to bytestring
    s = ""
    for smth in xrange(length):
        s = chr(i & 0xff) + s
        i >>= 8
    return s

def bytestring_to_int(s):
    # converts bytestring to integer
    i = 0
    for char in s:
        i <<= 8
        i |= ord(char)
    return i

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER

    #private key
    if content.startswith("-----BEGIN RSA PRIVATE KEY-----"):
        content = content.replace("-----BEGIN RSA PRIVATE KEY-----", "")
        content = content.replace("-----END RSA PRIVATE KEY-----", "")

    # public key
    if content.startswith("-----BEGIN PUBLIC KEY-----"):
        content = content.replace("-----BEGIN PUBLIC KEY-----", "")
        content = content.replace("-----END PUBLIC KEY-----", "")

    content = content.decode("base64")

    return content

def get_pubkey(filename):
    # reads public key file and returns (n, e)
    fileReader = open(filename, 'r')
    pubkeyRep = fileReader.read()
    fileReader.close()

    if pubkeyRep.startswith("-----"):
        pubkeyRep = pem_to_der(pubkeyRep)

    reprDER = decoder.decode(pubkeyRep)
    bitString = reprDER[0][1]

    octetsLength = len(bitString) / 8
    octets = 0

    for bit in bitString:
        octets = octets << 1 | bit

    pubkey = decoder.decode(int_to_bytestring(octets, octetsLength))[0]

    return int(pubkey[0]), int(pubkey[1])

def get_privkey(filename):
    # reads private key file and returns (n, d)
    fileReader = open(filename, 'r')
    privkeyRep = fileReader.read()
    fileReader.close()

    if privkeyRep.startswith("-----"):
        privkeyRep = pem_to_der(privkeyRep)

    privkey = decoder.decode(privkeyRep)

    return int(privkey[0][1]), int(privkey[0][3])


def pkcsv15pad_encrypt(plaintext, n):
    # pad plaintext for encryption according to PKCS#1 v1.5

    # calculate byte size of the modulus n
    k = 0
    while n:
        k = k + 1
        n = n >> 8

    textLength = len(plaintext) + 3 + 8
    additionalPad = k - textLength
    paddingLength = 8 + additionalPad

    # plaintext must be at least 11 bytes smaller than modulus
    randomPadding = os.urandom(paddingLength)

    while '\x00' in randomPadding:
        randomPadding = randomPadding.replace('\x00', os.urandom(1))

    # generate padding bytes
    padded_plaintext = '\x00' + '\x02' + randomPadding + '\x00' + plaintext
    return padded_plaintext

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5

    # calculate byte size of modulus n
    k = 0
    while n:
        k = k + 1
        n = n >> 8

    textLength = len(plaintext) + 3
    paddingLength = k - textLength

    # plaintext must be at least 3 bytes smaller than modulus
    padding = '\xFF'*paddingLength

    # generate padding bytes
    padded_plaintext = '\x00' + '\x01' + padding + '\x00' + plaintext
    return padded_plaintext

def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    paddingLength = 0
    if plaintext[0] == '\x00':
        paddingLength = 1
    else:
        return plaintext

    while plaintext[paddingLength] != '\x00':
        paddingLength += 1

    paddingLength += 1

    plaintext = plaintext[paddingLength:]

    return plaintext

def encrypt(keyfile, plaintextfile, ciphertextfile):
    n, e = get_pubkey(keyfile)

    textReader = open(plaintextfile, 'r')
    plainText = textReader.read()

    paddedText = pkcsv15pad_encrypt(plainText, n)

    m = bytestring_to_int(paddedText)

    c = pow(m, e, n)

    k = 0
    while n:
        k = k + 1
        n = n >> 8

    cipherText = int_to_bytestring(c, k)

    cipherReader = open(ciphertextfile, 'w')
    cipherReader.write(cipherText)

    pass

def decrypt(keyfile, ciphertextfile, plaintextfile):
    n, d = get_privkey(keyfile)

    cipherReader = open(ciphertextfile, 'r')
    cipherText = cipherReader.read()

    c = bytestring_to_int(cipherText)

    m = pow(c, d, n)

    k = 0
    while n:
        k = k + 1
        n = n >> 8

    paddedText = int_to_bytestring(m, k)

    plainText = pkcsv15pad_remove(paddedText)

    plainReader = open(plaintextfile, 'w')
    plainReader.write(plainText)

    pass

def digestinfo_der(filename):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA1 digest of file
    sha1 = hashlib.sha1()

    fileReader = open(filename, 'rb')
    data = fileReader.read(512)

    while data:
        sha1.update(data)
        data = fileReader.read(512)

    der = asn1_sequence(asn1_sequence(asn1_objectidentifier([1,3,14,3,2,26]) + asn1_null()) + asn1_octetstring(sha1.digest()))
    return der

def sign(keyfile, filetosign, signaturefile):
    n, d = get_privkey(keyfile)

    plainText = digestinfo_der(filetosign)

    paddedText = pkcsv15pad_sign(plainText, n)

    m = bytestring_to_int(paddedText)

    s = pow(m, d, n)

    k = 0
    while n:
        k = k + 1
        n = n >> 8

    signature = int_to_bytestring(s, k)

    signatureReader = open(signaturefile, 'w')
    signatureReader.write(signature)
    pass

def verify(keyfile, signaturefile, filetoverify):
    # prints "Verified OK" or "Verification Failure"
    n, e = get_pubkey(keyfile)

    signatureReader = open(signaturefile, 'rb')
    signature = signatureReader.read()

    s = bytestring_to_int(signature)

    m = pow(s, e, n)

    k = 0
    while n:
        k = k + 1
        n = n >> 8

    digestInfoPadded = int_to_bytestring(m, k)
    digestInfo = decoder.decode(pkcsv15pad_remove(digestInfoPadded))

    objectIdentifier = str(digestInfo[0][0][0])
    digestSHA1 = digestInfo[0][1]

    if objectIdentifier == "1.3.14.3.2.26":
        sha1 = hashlib.sha1()

        fileReader = open(filetoverify, 'rb')
        data = fileReader.read(512)

        while data:
            sha1.update(data)
            data = fileReader.read(512)

        computedSHA1 = sha1.digest()

        if digestSHA1 == computedSHA1:
            print "Verified OK"
        else:
            print "Verification Failure"

    else:
        print "Verification Failure"

    pass

def usage():
    print "Usage:"
    print "encrypt <public key file> <plaintext file> <output ciphertext file>"
    print "decrypt <private key file> <ciphertext file> <output plaintext file>"
    print "sign <private key file> <file to sign> <signature output file>"
    print "verify <public key file> <signature file> <file to verify>"
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'sign':
    sign(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'verify':
    verify(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
