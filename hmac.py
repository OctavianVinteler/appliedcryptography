#!/usr/bin/python

import hashlib, sys
sys.path.append('/usr/lib/python2.7/dist-packages/')
from pyasn1.codec.der import decoder

sys.path = sys.path[1:] # removes script directory from hmac.py search path
import hmac # do not use any other imports/libraries

# specify here how much time your solution required
# time = 7

####################################Encoder Functions##############################################

def int_to_bytestring(i):
    s = ""
    while i:
    	n = i & 0xff
    	s = chr(n) + s
    	i = i >> 8

    if s == "":
        s = chr(0x00)
    return s

def bytestring_to_int(s):
    i = 0
    for x in range (0, len(s)):
    	c = s[x]
    	i = (i << 8) + ord(c)
    return i

def asn1_len(content):
    numberOfBytes = len(content)
    if numberOfBytes <= 127:
    	return chr(numberOfBytes)
    else:
    	numberOfLengthBytes = int_to_bytestring(numberOfBytes)
    	return chr(128 | len(numberOfLengthBytes)) + numberOfLengthBytes

def asn1_boolean(bool):
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    typeRep = chr(0x02)
    rep = int_to_bytestring(i)

    temp = i
    while temp > 255:
        temp = temp >> 8

    if temp >= 128:
        rep = chr(0x00) + rep

    length = asn1_len(rep)
    
    return typeRep + length + rep

def asn1_bitstring(bitstr):
    typeRep = chr(0x03)
    padding = 8 - (len(bitstr) % 8)

    if padding == 8:
        padding = 0

    bitstr = bitstr + (padding*"0")

    if bitstr == "":
        rep = ""
    else:
        rep = int_to_bytestring(int(bitstr, 2))

    length = asn1_len(chr(padding) + rep)

    return typeRep + length + chr(padding) + rep

def asn1_octetstring(octets):
    typeRep = chr(0x04)
    length = asn1_len(octets)
    return typeRep + length + octets

def asn1_objectidentifier(oid):
    typeRep = chr(0x06)
    rep = ""

    if len(oid) >= 2:
        rep += chr(oid[0] * 40 + oid[1])
    elif len(oid) >= 1:
        rep += chr(oid[0] * 40)
    else:
        rep += chr(0x00)

    for index in range(2, len(oid)):
        number = oid[index]

        for x in range(10, 0, -1):
            power = pow(128,x)
            if number >= power:
                rep += chr(128 + (number / power))
                number = number - (number / power) * power

        rep += chr(oid[index] % 128)

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_sequence(der):
    typeRep = chr(0x30)
    length = asn1_len(der)
    
    return typeRep + length + der


def asn1_set(der):
    typeRep = chr(0x31)
    length = asn1_len(der)
    
    return typeRep + length + der

def asn1_printablestring(string):
    typeRep = chr(0x13)
    length = asn1_len(string)
    return typeRep + length + string

def asn1_utctime(time):
    typeRep = chr(0x17)
    length = asn1_len(time)
    return typeRep + length + time

def asn1_tag_explicit(der, tag):
    typeRep = chr(160 + tag)
    length = asn1_len(der)
    return typeRep + length + der

###################################################################################################

def verify(filename):
    print "[+] Reading HMAC DigestInfo from", filename+".hmac"

    hashedValue = hmac.new("")

    dataRead = ""
    fileReader = open(filename+".hmac", 'rb')
    data = fileReader.read(512)

    while data:
    	dataRead += data
    	data = fileReader.read(512)

    decodedData = decoder.decode(dataRead)
    algorithmIdentifier = (decodedData[0])[0]
    objectIdentifier = str(algorithmIdentifier[0])
    digest = (decodedData[0])[1]

    #MD5
    if objectIdentifier == "1.2.840.113549.2.5":
    	print "[+] HMAC-MD5 digest: " + hex(bytestring_to_int(digest))

    #SHA1
    if objectIdentifier == "1.3.14.3.2.26":
    	print "[+] HMAC-SHA1 digest: " + hex(bytestring_to_int(digest))

    #SHA256
    if objectIdentifier == "2.16.840.1.101.3.4.2.1":
    	print "[+] HMAC-SHA256 digest: " + hex(bytestring_to_int(digest))


    print "[?] Enter key:",
    key = raw_input()

    fileReader = open(filename, 'rb')
    data = fileReader.read(512)

    #MD5
    if objectIdentifier == "1.2.840.113549.2.5":
    	hashedValue = hmac.new(key, None, hashlib.md5)
    	while data:
	    	hashedValue.update(data)
	    	data = fileReader.read(512)

    	print "[+] Calculated HMAC-MD5: " + hashedValue.hexdigest()

    #SHA1
    if objectIdentifier == "1.3.14.3.2.26":
    	hashedValue = hmac.new(key, None, hashlib.sha1)
    	while data:
	    	hashedValue.update(data)
	    	data = fileReader.read(512)

    	print "[+] Calculated HMAC-SHA1: " + hashedValue.hexdigest()

    #SHA256
    if objectIdentifier == "2.16.840.1.101.3.4.2.1":
    	hashedValue = hmac.new(key, None, hashlib.sha256)
    	while data:
	    	hashedValue.update(data)
	    	data = fileReader.read(512)

    	print "[+] Calculated HMAC-SHA256: " + hashedValue.hexdigest()

    digest_calculated = hashedValue.digest()

    if digest_calculated != digest:
        print "[-] Wrong key or message has been manipulated!"
    else:
        print "[+] HMAC verification successful!"

def mac(filename):
    print "[?] Enter key:",
    key = raw_input()

    hashedValue = hmac.new(key, None, hashlib.sha256)

    fileReader = open(filename, 'rb')
    data = fileReader.read(512)

    while data:
    	hashedValue.update(data)
    	data = fileReader.read(512)

    print "[+] Calculated HMAC-256: " + hashedValue.hexdigest()

    asn1 = asn1_sequence(asn1_sequence(asn1_objectidentifier([2,16,840,1,101,3,4,2,1]) + asn1_null()) + asn1_octetstring(hashedValue.digest()))

    print "[+] Writing HMAC DigestInfo to", filename+".hmac"

    open(filename+".hmac", 'w').write(asn1)


def usage():
    print "Usage:"
    print "-verify <filename>"
    print "-mac <filename>"
    sys.exit(1)

if len(sys.argv) != 3:
    usage()
elif sys.argv[1] == '-mac':
    mac(sys.argv[2])
elif sys.argv[1] == '-verify':
    verify(sys.argv[2])
else:
    usage()
