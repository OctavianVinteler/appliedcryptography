#!/usr/bin/env python

import argparse, hashlib, sys, datetime # do not use any other imports/libraries


# specify here how much time your solution required
# time = 5 hours

# parse arguments
parser = argparse.ArgumentParser(description='Bitcoin Challenge')
parser.add_argument('--difficulty', required=True, type=int, help='Number of 0 bits')
args = parser.parse_args()

# converts integer to bytes (big endian)
def nb(i, length=0):
    bytes = ""
    for smth in xrange(length):
        bytes = chr(i & 0xff) + bytes
        i >>= 8
    return bytes

counter = -1

bits_hash = ""

start_time = datetime.datetime.utcnow()

challenge_solved = False

#Make compare references
complete_zero_bytes = (args.difficulty / 8)
zero_bytes = complete_zero_bytes * '\x00'

additional_zero_bits = args.difficulty % 8
bitstring = (additional_zero_bits * '0') + ((8 - additional_zero_bits) * '1')
int_rep = int(bitstring, 2)

while not challenge_solved:
    counter = counter + 1
    nonce = nb(counter, 8)

    input_value = "Octavian" + nonce
    #input_value = "Arnis UT" + nonce

    hash_value = hashlib.sha256(hashlib.sha256(input_value).digest()).digest()

    if hash_value[0:complete_zero_bytes] == zero_bytes:

        byte_value = ord(hash_value[complete_zero_bytes])

        if byte_value <= int_rep:
            end_time = datetime.datetime.utcnow()
            seconds = (end_time - start_time).total_seconds()
            print '[+] Solved in ' + str(seconds) + ' sec (' + str((counter / 1000000.0) / seconds) + ' Mhash/sec)'
            print '[+] Input: ' + input_value.encode('hex')
            print '[+] Solution: ' + hash_value.encode('hex')
            print '[+] Nonce: ' + str(counter)
            challenge_solved = True

#########################################Solutions###############################################

#For name = Arnis UT and difficulty 26
#[+] Solved in 413.957059 sec (0.151255840766 Mhash/sec)
#[+] Input: 41726e69732055540000000003bb67af
#[+] Solution: 00000031fc8ad63fa6070e341ccddd55bc36ac0b1e94965f2a8bb624d1a51071
#[+] Nonce: 62613423

#For name = Octavian and difficulty 26
#[+] Solved in 1.009301 sec (0.152755223665 Mhash/sec)
#[+] Input: 4f6374617669616e0000000000025a40
#[+] Solution: 00000012de8523e6d7881a15f69288e08e26f382a21955f4d0bdbdffc78cdda7
#[+] Nonce: 154176
