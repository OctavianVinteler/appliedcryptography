#!/usr/bin/env python

import argparse, hashlib, datetime, re, socket, sys, os, urlparse  # do not use any other imports/libraries
from pyasn1.codec.der import decoder, encoder

# specify here how much time your solution required
# time = 12

# parse arguments
parser = argparse.ArgumentParser(description='Check certificates against CRL', add_help=False)
parser.add_argument("url", type=str, help="URL of CRL (DER)")
parser.add_argument("--issuer", required=True, metavar='issuer', type=str, help="CA certificate that has issued certificates (DER/PEM)")
parser.add_argument("--certificates", required=True, metavar='cert', type=str, nargs='+', help="Certificates to check if revoked (DER/PEM)")
args = parser.parse_args()

####################################################### Asn1 Encoder ######################################################

def int_to_bytestring_asn1(i):
    s = ""
    while i:
        n = i & 0xff
        s = chr(n) + s
        i = i >> 8

    if s == "":
        s = chr(0x00)
    return s

def asn1_len(content):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    numberOfBytes = len(content)
    if numberOfBytes <= 127:
        #print "A: numberOfLengthBytes = " + str(numberOfBytes)
        return chr(numberOfBytes)
    else:
        numberOfLengthBytes = int_to_bytestring_asn1(numberOfBytes)
        return chr(128 | len(numberOfLengthBytes)) + numberOfLengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    typeRep = chr(0x02)
    rep = int_to_bytestring_asn1(i)

    temp = i
    while temp > 255:
        temp = temp >> 8

    if temp >= 128:
        rep = chr(0x00) + rep

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    typeRep = chr(0x03)
    padding = 8 - (len(bitstr) % 8)

    if padding == 8:
        padding = 0

    bitstr = bitstr + (padding*"0")

    if bitstr == "":
        rep = ""
    else:
        rep = int_to_bytestring_asn1(int(bitstr, 2))

    length = asn1_len(chr(padding) + rep)

    return typeRep + length + chr(padding) + rep

def asn1_bistring_bytestring(intRepr):
    typeRep = chr(0x03)
    padding = 0

    """bitstr = "";

    while intRepr > 0:
        byteRep = intRepr % 256
        intRepr = intRepr / 256
        while byteRep > 0:
            c = byteRep % 2
            bitstr = str(c) + bitstr
            byteRep = byteRep / 2

        lengthDiff = 8 - len(bitstr) % 8
        if lengthDiff == 8:
            lengthDiff = 0

        bitstr = lengthDiff*'0' + bitstr
    rep = int_to_bytestring_asn1(int(bitstr, 2))"""
    rep = int_to_bytestring_asn1(intRepr)

    length = asn1_len(chr(padding) + rep)

    return typeRep + length + chr(padding) + rep

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    typeRep = chr(0x04)
    length = asn1_len(octets)
    return typeRep + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    typeRep = chr(0x06)
    rep = ""

    if len(oid) >= 2:
        rep += chr(oid[0] * 40 + oid[1])
    elif len(oid) >= 1:
        rep += chr(oid[0] * 40)
    else:
        rep += chr(0x00)

    for index in range(2, len(oid)):
        number = oid[index]

        for x in range(10, 0, -1):
            power = pow(128,x)
            if number >= power:
                rep += chr(128 + (number / power))
                number = number - (number / power) * power

        rep += chr(oid[index] % 128)

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    typeRep = chr(0x30)
    length = asn1_len(der)

    return typeRep + length + der


def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    typeRep = chr(0x31)
    length = asn1_len(der)

    return typeRep + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    typeRep = chr(0x13)
    length = asn1_len(string)
    return typeRep + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    typeRep = chr(0x17)
    length = asn1_len(time)
    return typeRep + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    typeRep = chr(160 + tag)
    length = asn1_len(der)
    return typeRep + length + der

############################################################################################################################


def int_to_bytestring(i, length):
    # converts integer to bytestring
    s = ""
    for smth in xrange(length):
        s = chr(i & 0xff) + s
        i >>= 8
    return s

def bytestring_to_int(s):
    # converts bytestring to integer
    i = 0
    for char in s:
        i <<= 8
        i |= ord(char)
    return i

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    if content[:2] == '--':
        content = content.replace("-----BEGIN CERTIFICATE REQUEST-----", "")
        content = content.replace("-----END CERTIFICATE REQUEST-----", "")
        content = content.replace("-----BEGIN CERTIFICATE-----", "")
        content = content.replace("-----END CERTIFICATE-----", "")
        content = content.replace("-----BEGIN PUBLIC KEY-----", "")
        content = content.replace("-----END PUBLIC KEY-----", "")
        content = content.replace("-----BEGIN RSA PRIVATE KEY-----", "")
        content = content.replace("-----END RSA PRIVATE KEY-----", "")
        content = content.decode('base64')
    return content

def get_pubkey_certificate(filename):
    # reads certificate and returns (n, e) from subject public key
    fileReader = open(filename, 'r')
    certificate = fileReader.read()
    fileReader.close()

    if certificate.startswith("-----"):
        certificate = pem_to_der(certificate)

    certificateRep = decoder.decode(certificate)

    publicKeyBits = certificateRep[0][0][6][1]

    octetsLength = len(publicKeyBits) / 8
    octets = 0

    for bit in publicKeyBits:
        octets = octets << 1 | bit

    pubKeyRep = decoder.decode(int_to_bytestring(octets, octetsLength))[0]

    return int(pubKeyRep[0]), int(pubKeyRep[1])



def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    plaintext = plaintext[2:]
    if "\x00" in plaintext:
        return plaintext[plaintext.index("\x00")+1:]

def digestinfo_der(m):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA1 digest of m
    sha1 = hashlib.sha1()

    sha1.update(m)

    der = asn1_sequence(asn1_sequence(asn1_objectidentifier([1,3,14,3,2,26]) + asn1_null()) + asn1_octetstring(sha1.digest()))

    return der

def verify(certfile, c, contenttoverify):
    # returns 1 on "Verified OK" and 0 otherwise
    n, e = get_pubkey_certificate(certfile)

    m = pow(c, e, n)

    k = 0
    while n:
        k = k + 1
        n = n >> 8

    m = int_to_bytestring(m, k)
    m = pkcsv15pad_remove(m)

    digestinfo = digestinfo_der(contenttoverify)

    if m == digestinfo:
        return 1
    return 0


# list of serial numbers to check
serials = []

# download CRL using python sockets
def download_crl(url):

    print "[+] Downloading", url

    # parsing url
    parseResult = urlparse.urlparse(url)
    netloc = parseResult.netloc
    path = parseResult.path

    # get response header
    requestString = "GET " + path + " HTTP/1.1\r\nHost: " + netloc + "\r\n\r\n"

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((netloc, 80))
    s.send(requestString)

    header = ""

    while True:
        buff = s.recv(1)
        if not buff:
            print "[-] Connection Closed - Program exiting..."
            sys.exit(1)
        header = header + buff
        if header[-4:] == "\r\n\r\n":
            break;

    # get content-length value
    contentLength = int(re.search('content-length:\s*(\d+)\s', header, re.S+re.I).group(1), 10)

    # receive CRL (get response body)
    crl = ""
    for i in range(contentLength):
        buff = s.recv(1)
        if not buff:
            print "[-] Connection Closed - Program exiting..."
            sys.exit(1)
        crl = crl + buff

    return crl

# verify if the CRL is signed by the issuer and whether the CRL is fresh
def verify_crl(issuer, crl):
    """cacert = open(issuer).read()

    if cacert.startswith("-----"):
        cacert = pem_to_der(cacert)

    cacertDER = decoder.decode(cacert)"""


    tbsCertList = encoder.encode(decoder.decode(crl)[0][0])

    signatureBits = decoder.decode(crl)[0][2]

    octetsLength = len(signatureBits) / 8
    signature = 0

    for bit in signatureBits:
        signature = signature << 1 | bit

    if verify(issuer, signature, tbsCertList):
        print "[+] CRL signature check successful!"
    else:
        print "[-] CRL signature verification failed!"
        sys.exit(1)

    thisUpdate = datetime.datetime.strptime(str(decoder.decode(crl)[0][0][3]), '%y%m%d%H%M%SZ')
    nextUpdate = datetime.datetime.strptime(str(decoder.decode(crl)[0][0][4]), '%y%m%d%H%M%SZ')
    now = datetime.datetime.utcnow()

    if now < thisUpdate or now > nextUpdate:
        print "[-] CRL outdated (nextUpdate: %s) (now: %s)" % (nextUpdate, now)
        sys.exit(1)


# verify if the certificates are signed by the issuer and add them to the list 'serials'
def load_serials(issuer, certificates):
    global serials

    for certificate in args.certificates:

        cert = open(certificate, 'r').read()

        certificateDER = pem_to_der(cert)

        tbsCertificate = encoder.encode(decoder.decode(certificateDER)[0][0])
        serial = decoder.decode(certificateDER)[0][0][1]

        signatureBits = decoder.decode(certificateDER)[0][2]

        octetsLength = len(signatureBits) / 8
        signature = 0

        for bit in signatureBits:
            signature = signature << 1 | bit

        if verify(issuer, signature, tbsCertificate):
            print "[+] Serial %s (%s) loaded" % (serial, certificate)
            serials.append(serial)
        else:
            print "[-] Serial %s (%s) not loaded: not issued by CA" % (serial, certificate)


# check if the certificates are revoked
# if revoked -- print revocation date and reason (if available)
def check_revoked(crl):
    global serials

    CRLReason = {
	0: 'unspecified',
	1: 'keyCompromise',
	2: 'cACompromise',
	3: 'affiliationChanged',
	4: 'superseded',
	5: 'cessationOfOperation',
	6: 'certificateHold',
	8: 'removeFromCRL',
	9: 'privilegeWithdrawn',
	10: 'aACompromise',
	}

    # loop over revokedCerts
    for revokedCert in decoder.decode(crl)[0][0][5]:

        serial = revokedCert[0]

        # if revoked serial is in our interest
        if serial in serials:

            revocationDate = datetime.datetime.strptime(str(revokedCert[1]), '%y%m%d%H%M%SZ')

            reasonCode = decoder.decode(revokedCert[2][0][1])[0]

            reason = CRLReason[reasonCode]

            # check if the extensions are present
            extensions = decoder.decode(crl)[0][0][6]

            if extensions is not None:
                print "[-] Certificate %s revoked: %s %s" % (serial, revocationDate, reason)




# download CRL
crl = download_crl(args.url)

# verify CRL
verify_crl(args.issuer, crl)

# load serial numbers from valid certificates
load_serials(args.issuer, args.certificates)

# check revocation status
check_revoked(crl)
