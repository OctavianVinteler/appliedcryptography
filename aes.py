#!/usr/bin/python

import datetime, os, sys
from pyasn1.codec.der import decoder

# $ sudo apt-get install python-crypto
sys.path = sys.path[1:] # removes script directory from aes.py search path
from Crypto.Cipher import AES          # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Cipher.AES-module.html
from Crypto.Protocol.KDF import PBKDF2 # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Protocol.KDF-module.html#PBKDF2
from Crypto.Util.strxor import strxor  # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Util.strxor-module.html#strxor
import hashlib, hmac # do not use any other imports/libraries


# specify here how much time your solution required
#time = 11.5 hours

##########################################Helper Functions################################################
def int_to_bytestring(i):
    s = ""
    while i:
        n = i & 0xff
        s = chr(n) + s
        i = i >> 8

    if s == "":
        s = chr(0x00)
    return s

def bytestring_to_int(s):
    i = 0
    for x in range (0, len(s)):
        c = s[x]
        i = (i << 8) + ord(c)
    return i
##########################################################################################################

####################################DER Encoder Functions#################################################

def int_to_bytestring(i):
    s = ""
    while i:
        n = i & 0xff
        s = chr(n) + s
        i = i >> 8

    if s == "":
        s = chr(0x00)
    return s

def bytestring_to_int(s):
    i = 0
    for x in range (0, len(s)):
        c = s[x]
        i = (i << 8) + ord(c)
    return i

def asn1_len(content):
    numberOfBytes = len(content)
    if numberOfBytes <= 127:
        return chr(numberOfBytes)
    else:
        numberOfLengthBytes = int_to_bytestring(numberOfBytes)
        return chr(128 | len(numberOfLengthBytes)) + numberOfLengthBytes

def asn1_boolean(bool):
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    typeRep = chr(0x02)
    rep = int_to_bytestring(i)

    temp = i
    while temp > 255:
        temp = temp >> 8

    if temp >= 128:
        rep = chr(0x00) + rep

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_bitstring(bitstr):
    typeRep = chr(0x03)
    padding = 8 - (len(bitstr) % 8)

    if padding == 8:
        padding = 0

    bitstr = bitstr + (padding*"0")

    if bitstr == "":
        rep = ""
    else:
        rep = int_to_bytestring(int(bitstr, 2))

    length = asn1_len(chr(padding) + rep)

    return typeRep + length + chr(padding) + rep

def asn1_octetstring(octets):
    typeRep = chr(0x04)
    length = asn1_len(octets)
    return typeRep + length + octets

def asn1_objectidentifier(oid):
    typeRep = chr(0x06)
    rep = ""

    if len(oid) >= 2:
        rep += chr(oid[0] * 40 + oid[1])
    elif len(oid) >= 1:
        rep += chr(oid[0] * 40)
    else:
        rep += chr(0x00)

    for index in range(2, len(oid)):
        number = oid[index]

        for x in range(10, 0, -1):
            power = pow(128,x)
            if number >= power:
                rep += chr(128 + (number / power))
                number = number - (number / power) * power

        rep += chr(oid[index] % 128)

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_sequence(der):
    typeRep = chr(0x30)
    length = asn1_len(der)

    return typeRep + length + der


def asn1_set(der):
    typeRep = chr(0x31)
    length = asn1_len(der)

    return typeRep + length + der

def asn1_printablestring(string):
    typeRep = chr(0x13)
    length = asn1_len(string)
    return typeRep + length + string

def asn1_utctime(time):
    typeRep = chr(0x17)
    length = asn1_len(time)
    return typeRep + length + time

def asn1_tag_explicit(der, tag):
    typeRep = chr(160 + tag)
    length = asn1_len(der)
    return typeRep + length + der
##########################################################################################################

# this function benchmarks how many PBKDF2 iterations
# can be performed in one second on the machine it is executed
def benchmark():

    salt = os.urandom(8)

    # measure time for performing 10000 iterations
    start = datetime.datetime.now()

    PBKDF2("password", salt, 10000, 36)

    stop = datetime.datetime.now()

    time = (stop - start).total_seconds()

    # extrapolate to 1 second

    iter = int(10000 / (time / 1))

    print "[+] Benchmark: %s PBKDF2 iterations in 1 second" % (iter)

    return iter # returns number of iterations that can be performed in 1 second


def encrypt(pfile, cfile):

    # benchmarking
    iter = benchmark()

    # asking for password
    print "[?] Enter password:",
    password = raw_input()

    # derieving key
    salt = os.urandom(8)

    #derivedKey = PBKDF2(password, salt, iter, 36)
    derivedKey = PBKDF2(password, salt, 36, iter)
    aes128key = derivedKey[:16]
    hmacSHA1key = derivedKey[16:]

    # writing ciphertext in temporary file and calculating HMAC digest
    iv = os.urandom(16)

    fileReader = open(pfile, 'rb')
    tmpFileReader = open('tmpfile', 'w').close()
    tmpFileReader = open("tmpfile", 'a')

    plainBlock = fileReader.read(16)
    cipherBlock = iv
    cipher = AES.new(aes128key)

    padding = False

    while plainBlock:

        #the last block of the file
        if (len(plainBlock) != 16):
            bytesDiff = 16 - len(plainBlock)
            if bytesDiff > 0:
                padChr = chr(bytesDiff)
                padding = True

            for i in xrange(bytesDiff):
                plainBlock = plainBlock + padChr

        #xorResult = int_to_bytestring(bytestring_to_int(plainBlock) ^ bytestring_to_int(cipherBlock))
        xorResult = strxor(plainBlock, cipherBlock)

        #fix for the case when the first characters of the plainBlock and cipherBlock are the same, therefore the
        #result of the xor result is 0 for the first character in the block
        """if len(xorResult) == 15:
            xorResult = chr(0x00) + xorResult"""

        cipherBlock = cipher.encrypt(xorResult)
        tmpFileReader.write(cipherBlock)

        plainBlock = fileReader.read(16)

    if padding == False:
        #plainBlock = "\x010x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f"
        plainBlock = chr(16)*16
        #xorResult = int_to_bytestring(bytestring_to_int(plainBlock) ^ bytestring_to_int(cipherBlock))
        xorResult = strxor(plainBlock, cipherBlock)

        """if len(xorResult) == 15:
            xorResult = chr(0x00) + xorResult"""

        cipherBlock = cipher.encrypt(xorResult)
        tmpFileReader.write(cipherBlock)

    fileReader.close()
    tmpFileReader.close()

    # writing DER structure in cfile
    hashedValue = hmac.new(hmacSHA1key, None, hashlib.sha1)

    fileReader = open("tmpfile", 'rb')
    data = fileReader.read(512)

    while data:
        hashedValue.update(data)
        data = fileReader.read(512)

    parameters = asn1_sequence(asn1_sequence(asn1_octetstring(salt) + asn1_integer(iter) + asn1_integer(36)) + asn1_sequence(asn1_objectidentifier([2,16,840,1,101,3,4,1,2]) + asn1_octetstring(iv)) + asn1_sequence(asn1_sequence(asn1_objectidentifier([1,3,14,3,2,26]) + asn1_null()) + asn1_octetstring(hashedValue.digest())))

    cipherFileReader = open(cfile, 'w')
    cipherFileReader.write(parameters)
    cipherFileReader.close()

    # append temporary ciphertext file to cfile
    tmpFileReader = open("tmpfile", 'r')
    cipherFileReader = open(cfile, 'a')

    data = tmpFileReader.read(512)

    while data:
        cipherFileReader.write(data)
        data = tmpFileReader.read(512)

    # deleting temporary ciphertext file
    os.remove("tmpfile")

    pass


def decrypt(cfile, pfile):


    # reading DER structure
    cipherFileReader = open(cfile, 'rb')
    cipherFileReader.read(1)
    lengthDER = bytestring_to_int(cipherFileReader.read(1))

    if lengthDER >= 128:
        lengthDER = bytestring_to_int(lengthDER) & 127
        firstBytes = cipherFileReader.read(lengthDER)
        lengthDER = 0
        for c in firstBytes:
            lengthDER = (lengthDER << 8) + ord(c)

    cipherFileReader.close()
    cipherFileReader = open(cfile, 'rb')

    parameters = cipherFileReader.read(lengthDER + 2)
    cipherFileReader.close()

    decodedParameters = decoder.decode(parameters)

    pbkdf2 = decodedParameters[0][0]
    aesInfo = decodedParameters[0][1]
    digestInfo = decodedParameters[0][2]

    salt = str(pbkdf2[0])
    iter = int(pbkdf2[1])
    keyLength = int(pbkdf2[2])

    cipherIdentifier = str(aesInfo[0])
    iv = str(aesInfo[1])

    algorithmIdentifier = str(digestInfo[0][0])
    hmacDigest =str(digestInfo[1])

    # asking for password
    print "[?] Enter password:",
    password = raw_input()

    # derieving key
    derivedKey = PBKDF2(password, salt, 36, iter)
    aes128key = derivedKey[:16]
    hmacSHA1key = derivedKey[16:]

    # first pass over ciphertext to calculate and verify HMAC
    if algorithmIdentifier == "1.3.14.3.2.26":
        hashedValue = hmac.new(hmacSHA1key, None, hashlib.sha1)

        cipherFileReader = open(cfile, 'rb')
        cipherFileReader.read(lengthDER + 2)
        cipheredData = cipherFileReader.read(512)

        while cipheredData:
            hashedValue.update(cipheredData)
            cipheredData = cipherFileReader.read(512)

        digest_calculated = hashedValue.digest()

        if digest_calculated != hmacDigest:
            print "[-] HMAC verification failure: wrong password or modified ciphertext!"
            return
        else:
            print "[+] HMAC verification successful!"
    else:
        print "[-] Unknown function used for HMAC encoding!"

    # second pass over ciphertext to decrypt
    #decryptedFileReader = open(pfile, 'w').close()
    #decryptedFileReader = open(pfile, 'a')
    decryptedFileReader = open(pfile, 'w')
    cipherFileReader = open(cfile, 'r')
    #cipherFileReader.readline()
    cipherFileReader.seek(lengthDER + 2)

    cipher = AES.new(aes128key)

    cipherBlock = cipherFileReader.read(16)
    xorBlock = iv

    while cipherBlock:

        nextBlock = cipherFileReader.read(16)

        decryptedBlock = cipher.decrypt(cipherBlock)
        #plainBlock = int_to_bytestring(bytestring_to_int(decryptedBlock) ^ bytestring_to_int(xorBlock))
        plainBlock = strxor(decryptedBlock, xorBlock)

        if not len(nextBlock):
            c = ord(plainBlock[-1])

            """if c >= 15:
                plainBlock = ""
            else:
                plainBlock = plainBlock[0:-c]"""
            plainBlock = plainBlock[0:16-c]

        decryptedFileReader.write(plainBlock)

        xorBlock = cipherBlock

        cipherBlock = nextBlock

    decryptedFileReader.close()

    pass

def usage():
    print "Usage:"
    print "-encrypt <plaintextfile> <ciphertextfile>"
    print "-decrypt <ciphertextfile> <plaintextfile>"
    sys.exit(1)


if len(sys.argv) != 4:
    usage()
elif sys.argv[1] == '-encrypt':
    encrypt(sys.argv[2], sys.argv[3])
elif sys.argv[1] == '-decrypt':
    decrypt(sys.argv[2], sys.argv[3])
else:
    usage()
