#!/usr/bin/env python
import os, sys       # do not use any other imports/libraries
# specify here how much time your solution required
# time spent = 2.5 hours

def bytestring_to_int(s):
    i = 0
    for x in range (0, len(s)):
    	c = s[x]
    	i = (i << 8) + ord(c)
    return i

def int_to_bytestring(i, length):
    s = ""
    for x in range (0, length):
    	n = i & 0xff
    	s = chr(n) + s
    	i = i >> 8
    return s


def encrypt(pfile, kfile, cfile):
    plainText = open(pfile).read()
    bigIntText = bytestring_to_int(plainText)
    randomKey = os.urandom(len(plainText))
    bigIntKey = bytestring_to_int(randomKey)
    cypherInt = bigIntText ^ bigIntKey
    cypher = int_to_bytestring(cypherInt, len(plainText))
    open(kfile, 'w').write(randomKey)
    open(cfile, 'w').write(cypher)
    pass
    

def decrypt(cfile, kfile, pfile):
    cypher = open(cfile).read()
    bigIntCypher = bytestring_to_int(cypher)
    key = open(kfile).read()
    bigIntKey = bytestring_to_int(key)
    bigIntText = bigIntCypher ^ bigIntKey
    plainText = int_to_bytestring(bigIntText, len(key))
    open(pfile, 'w').write(plainText)
    pass

def usage():
    print "Usage:"
    print "encrypt <plaintext file> <output key file> <ciphertext output file>"
    print "decrypt <ciphertext file> <key file> <plaintext output file>"
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()

#encrypt("plain_text.txt","key_text.txt","cypher_text.txt")
#decrypt("cypher_text.txt","key_text.txt","decrypted_text.txt")