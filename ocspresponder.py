#!/usr/bin/env python

import argparse, datetime, hashlib, re, socket, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder, encoder

# specify here how much time your solution required
# time = 14 hours

# parse arguments
parser = argparse.ArgumentParser(description='OCSP responder', add_help=False)
parser.add_argument("--privkey", required=True, metavar='privkey', type=str, help="CA private key (DER/PEM)")
parser.add_argument("--cacert", required=True, metavar='cacert', type=str, help="CA certificate (DER/PEM)")
parser.add_argument("--revoked", required=True, metavar='cert', type=str, nargs='+', help="Revoked certificates (DER/PEM)")
args = parser.parse_args()

####################################################### Asn1 Encoder ######################################################

def int_to_bytestring_asn1(i):
    s = ""
    while i:
        n = i & 0xff
        s = chr(n) + s
        i = i >> 8

    if s == "":
        s = chr(0x00)
    return s

def asn1_len(content):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    numberOfBytes = len(content)
    if numberOfBytes <= 127:
        return chr(numberOfBytes)
    else:
        numberOfLengthBytes = int_to_bytestring_asn1(numberOfBytes)
        return chr(128 | len(numberOfLengthBytes)) + numberOfLengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    typeRep = chr(0x02)
    rep = int_to_bytestring_asn1(i)

    temp = i
    while temp > 255:
        temp = temp >> 8

    if temp >= 128:
        rep = chr(0x00) + rep

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_enumerated(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of ENUMERATED
    typeRep = chr(0x0A)
    rep = int_to_bytestring_asn1(i)

    temp = i
    while temp > 255:
        temp = temp >> 8

    if temp >= 128:
        rep = chr(0x00) + rep

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    typeRep = chr(0x03)
    padding = 8 - (len(bitstr) % 8)

    if padding == 8:
        padding = 0

    bitstr = bitstr + (padding*"0")

    if bitstr == "":
        rep = ""
    else:
        rep = int_to_bytestring_asn1(int(bitstr, 2))

    length = asn1_len(chr(padding) + rep)

    return typeRep + length + chr(padding) + rep

def asn1_bistring_bytestring(intRepr):
    typeRep = chr(0x03)
    padding = 0

    length = asn1_len(chr(padding) + intRepr)

    return typeRep + length + chr(padding) + intRepr

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    typeRep = chr(0x04)
    length = asn1_len(octets)
    return typeRep + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    typeRep = chr(0x06)
    rep = ""

    if len(oid) >= 2:
        rep += chr(oid[0] * 40 + oid[1])
    elif len(oid) >= 1:
        rep += chr(oid[0] * 40)
    else:
        rep += chr(0x00)

    for index in range(2, len(oid)):
        number = oid[index]

        for x in range(10, 0, -1):
            power = pow(128,x)
            if number >= power:
                rep += chr(128 + (number / power))
                number = number - (number / power) * power

        rep += chr(oid[index] % 128)

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    typeRep = chr(0x30)
    length = asn1_len(der)

    return typeRep + length + der


def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    typeRep = chr(0x31)
    length = asn1_len(der)

    return typeRep + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    typeRep = chr(0x13)
    length = asn1_len(string)
    return typeRep + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    typeRep = chr(0x17)
    length = asn1_len(time)
    return typeRep + length + time

def asn1_generalizedtime(time):
    # returns DER encoding of UTCTime
    typeRep = chr(0x18)
    length = asn1_len(time)
    return typeRep + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    typeRep = chr(160 + tag)
    length = asn1_len(der)
    return typeRep + length + der

def asn1_tag_implicit(der, tag):
    formBit = ord(der[0]) & 32
    typeRep = chr(128 + formBit + tag)
    return typeRep + der[1:]

############################################################################################################################


def int_to_bytestring(i, length):
    # converts integer to bytestring
    s = ""
    for smth in xrange(length):
        s = chr(i & 0xff) + s
        i >>= 8
    return s

def bytestring_to_int(s):
    # converts bytestring to integer
    i = 0
    for char in s:
        i <<= 8
        i |= ord(char)
    return i

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    if content[:2] == '--':
        content = content.replace("-----BEGIN CERTIFICATE REQUEST-----", "")
        content = content.replace("-----END CERTIFICATE REQUEST-----", "")
        content = content.replace("-----BEGIN CERTIFICATE-----", "")
        content = content.replace("-----END CERTIFICATE-----", "")
        content = content.replace("-----BEGIN PUBLIC KEY-----", "")
        content = content.replace("-----END PUBLIC KEY-----", "")
        content = content.replace("-----BEGIN RSA PRIVATE KEY-----", "")
        content = content.replace("-----END RSA PRIVATE KEY-----", "")
        content = content.decode('base64')
    return content

def get_pubkey_cert(cert):
    # reads certificate and returns subjectPublicKey
    certificate = open(cert, 'r').read()

    if certificate.startswith("-----"):
        certificate = pem_to_der(certificate)

    certificateRep = decoder.decode(certificate)

    publicKeyBits = certificateRep[0][0][6][1]

    octetsLength = len(publicKeyBits) / 8
    octets = 0

    for bit in publicKeyBits:
        octets = octets << 1 | bit

    return int_to_bytestring(octets, octetsLength)

def get_privkey(filename):
    # reads private key file and returns (n, d)
    privkeyRep = open(filename, 'r').read()

    if privkeyRep.startswith("-----"):
        privkeyRep = pem_to_der(privkeyRep)

    privkey = decoder.decode(privkeyRep)

    return int(privkey[0][1]), int(privkey[0][3])


def digestinfo_der(m):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA1 digest of file
    sha1 = hashlib.sha1()

    sha1.update(m)

    der = asn1_sequence(asn1_sequence(asn1_objectidentifier([1,3,14,3,2,26]) + asn1_null()) + asn1_octetstring(sha1.digest()))

    return der

def sign(m, keyfile):
    n, d = get_privkey(keyfile)

    s = pow(m, d, n)

    k = 0
    while n:
        k = k + 1
        n = n >> 8

    c = int_to_bytestring(s, k)
    return c

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5

    # calculate byte size of modulus n
    k = 0
    while n:
        k = k + 1
        n = n >> 8

    textLength = len(plaintext) + 3
    paddingLength = k - textLength

    # plaintext must be at least 3 bytes smaller than modulus
    padding = '\xFF'*paddingLength

    # generate padding bytes
    padded_plaintext = '\x00' + '\x01' + padding + '\x00' + plaintext
    return padded_plaintext


# serial numbers of revoked certificates
serials = []

# in a loop obtain DER encoded OCSP request and send response
def process_requests():

    sserv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sserv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sserv.bind(('', 8888))
    sserv.listen(0)

    while True:
        (s, address) = sserv.accept()
        print "[+] Connection from %s:%s" % (address[0], address[1])

        # read HTTP request header
        header = ""

        while True:
            buff = s.recv(1)
            if not buff:
                print "[-] Connection Closed - Program exiting..."
                sys.exit(1)
            header = header + buff
            if header[-4:] == "\r\n\r\n":
                break;

        # send error message if GET request (bonus)
        if header.startswith('GET'):
            answer = "The server is processing only OCSP POST requests!"
            header = "HTTP/1.1 405 Not Allowed\r\n" + "Date: "+str(datetime.datetime.utcnow())+"\r\n" + "Connection: close\r\n" + "Content-Length: " + str(len(answer)) + "\r\n\r\n"
            s.send(header + answer)
            s.close()
            return

        # read OCSP request
        contentLength = int(re.search('content-length:\s*(\d+)\s', header, re.S+re.I).group(1), 10)

        ocspRequest = ""
        for i in range(contentLength):
            buff = s.recv(1)
            if not buff:
                print "[-] Connection Closed - Program exiting..."
                sys.exit(1)
            ocspRequest = ocspRequest + buff

        # send OCSP response
        response = produce_response(ocspRequest, address[0])

        header = "HTTP/1.1 200 OK\r\n" + "Date: "+str(datetime.datetime.utcnow())+"\r\n" + "Connection: close\r\n" + "Content-Length: " + str(len(response)) + "\r\n\r\n"

        answer = header + response

        s.send(answer)
        s.close()

# load serials of revoked certificates to list 'serials'
def load_serials(certificates):
    global serials

    for certificate in certificates:

        cert = open(certificate, 'r').read()

        certificateDER = pem_to_der(cert)

        serial = decoder.decode(certificateDER)[0][0][1]

        print "[+] Serial %s (%s) loaded" % (serial, certificate)

        serials.append(serial)

def composeResponseData(name, certID, certStatus, thisUpdate, nonce):
    if nonce == "":
        return asn1_sequence(asn1_tag_explicit(name, 1) + asn1_generalizedtime(datetime.datetime.utcnow().strftime("%Y%m%d%H%M%SZ")) + asn1_sequence(asn1_sequence(certID + certStatus + thisUpdate)))
    else:
        return asn1_sequence(asn1_tag_explicit(name, 1) + asn1_generalizedtime(datetime.datetime.utcnow().strftime("%Y%m%d%H%M%SZ")) + asn1_sequence(asn1_sequence(certID + certStatus + thisUpdate)) + nonce)

# produce OCSP response for DER encoded request
def produce_response(req, ip):
    global args, serials

    open("request", 'w').write(req)

    # return unauthorized(6) if non-localhost client (bonus)
    if '127.0.0.1' not in ip:
        return asn1_sequence(asn1_enumerated(6))

    # get subject name from CA certificate
    cacert = open(args.cacert).read()

    if cacert.startswith('-----'):
        cacert = pem_to_der(cacert)

    #subjectName = decoder.decode(cacert)[0][0][5][3][0][1]
    subjectName = encoder.encode(decoder.decode(cacert)[0][0][5])

    # get subjectPublicKey (not subjectPublicKeyInfo) from CA certificate
    pubKey = get_pubkey_cert(args.cacert)

    # get serial number from request
    serial = decoder.decode(req)[0][0][0][0][0][3]

    # calculate SHA1 hash of CA subject name and CA public key (return CertStatus 'unknown' if not issued by CA)
    sha1 = hashlib.sha1()
    sha1.update(subjectName)
    subjectNameHash = sha1.digest()

    issuerNameHash = str(decoder.decode(req)[0][0][0][0][0][1])

    certStatus = ""

    if subjectNameHash != issuerNameHash:
        certStatus = asn1_tag_implicit(asn1_null(), 2)

    sha1 = hashlib.sha1()
    sha1.update(pubKey)
    pubkeyHash = sha1.digest()

    issuerKeyHash = str(decoder.decode(req)[0][0][0][0][0][2])

    if pubkeyHash != issuerKeyHash:
        certStatus = asn1_tag_implicit(asn1_null(), 2)

    # return 'revoked' CertStatus if serial in serials (otherwise return 'good')
    if serial in serials:
        certStatus = asn1_tag_implicit(asn1_sequence(asn1_generalizedtime(datetime.datetime.utcnow().strftime("%Y%m%d%H%M%SZ")) + asn1_tag_explicit(asn1_enumerated(1), 0)), 1)

    if certStatus == "":
        certStatus = asn1_tag_implicit(asn1_null(), 0)

    # nonce extension (bonus)
    nonce = ""
    if len(decoder.decode(req)[0][0]) == 2:
        nonce = asn1_tag_explicit(asn1_sequence(encoder.encode(decoder.decode(req)[0][0][1][0])), 1)

    name = encoder.encode(decoder.decode(cacert)[0][0][5])
    certID = encoder.encode(decoder.decode(req)[0][0][0][0][0])
    thisUpdate = asn1_generalizedtime(datetime.datetime.utcnow().strftime("%Y%m%d%H%M%SZ"))

    tbsResponseData = composeResponseData(name, certID, certStatus, thisUpdate, nonce)

    # get signature of tbsResponseData
    n, d = get_privkey(args.privkey)
    paddedText = pkcsv15pad_sign(digestinfo_der(tbsResponseData), n)
    signature = sign(bytestring_to_int(paddedText), args.privkey)

    resp = asn1_sequence(asn1_enumerated(0) + asn1_tag_explicit(asn1_sequence(asn1_objectidentifier([1,3,6,1,5,5,7,48,1,1]) + asn1_octetstring(asn1_sequence(tbsResponseData + asn1_sequence(asn1_objectidentifier([1,2,840,113549,1,1,5]) + asn1_null()) + asn1_bistring_bytestring(signature)))), 0))

    # return DER encoded OCSP response
    open("respFile", 'w').write(resp)
    return resp

load_serials(args.revoked)
process_requests()
