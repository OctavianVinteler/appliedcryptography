#!/usr/bin/env python
import sys   # do not use any other imports/libraries

# specify here how much time your solution required
# Time = 11

def int_to_bytestring(i):
    s = ""
    while i:
    	n = i & 0xff
    	s = chr(n) + s
    	i = i >> 8

    if s == "":
        s = chr(0x00)
    return s

def bytestring_to_int(s):
    i = 0
    for x in range (0, len(s)):
    	c = s[x]
    	i = (i << 8) + ord(c)
    return i

def asn1_len(content):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    numberOfBytes = len(content)
    if numberOfBytes <= 127:
        #print "A: numberOfLengthBytes = " + str(numberOfBytes)
    	return chr(numberOfBytes)
    else:
    	numberOfLengthBytes = int_to_bytestring(numberOfBytes)
        #print "B: numberOfLengthBytes = " + str(bytestring_to_int(chr(128 | len(numberOfLengthBytes)) + numberOfLengthBytes))
    	return chr(128 | len(numberOfLengthBytes)) + numberOfLengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    typeRep = chr(0x02)
    rep = int_to_bytestring(i)

    temp = i
    while temp > 255:
        temp = temp >> 8

    if temp >= 128:
        rep = chr(0x00) + rep

    length = asn1_len(rep)
    
    return typeRep + length + rep

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    typeRep = chr(0x03)
    padding = 8 - (len(bitstr) % 8)

    if padding == 8:
        padding = 0

    bitstr = bitstr + (padding*"0")

    if bitstr == "":
        rep = ""
    else:
        rep = int_to_bytestring(int(bitstr, 2))

    length = asn1_len(chr(padding) + rep)

    return typeRep + length + chr(padding) + rep

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    typeRep = chr(0x04)
    length = asn1_len(octets)
    return typeRep + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    typeRep = chr(0x06)
    rep = ""

    if len(oid) >= 2:
        rep += chr(oid[0] * 40 + oid[1])
    elif len(oid) >= 1:
        rep += chr(oid[0] * 40)
    else:
        rep += chr(0x00)

    for index in range(2, len(oid)):
        number = oid[index]

        for x in range(10, 0, -1):
            power = pow(128,x)
            if number >= power:
                rep += chr(128 + (number / power))
                number = number - (number / power) * power

        rep += chr(oid[index] % 128)

    length = asn1_len(rep)

    return typeRep + length + rep

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    typeRep = chr(0x30)
    length = asn1_len(der)
    
    return typeRep + length + der


def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    typeRep = chr(0x31)
    length = asn1_len(der)
    
    return typeRep + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    typeRep = chr(0x13)
    length = asn1_len(string)
    return typeRep + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    typeRep = chr(0x17)
    length = asn1_len(time)
    return typeRep + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    typeRep = chr(160 + tag)
    length = asn1_len(der)
    return typeRep + length + der

# figure out what to put in '...' by looking on ASN.1 structure required
asn1 = asn1_tag_explicit(asn1_sequence(asn1_set(asn1_integer(5) + asn1_tag_explicit(asn1_integer(200), 2) + asn1_tag_explicit(asn1_integer(65407), 11)) + asn1_boolean(True) + asn1_bitstring("110") + asn1_octetstring("\x00\x01\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02") + asn1_null() + asn1_objectidentifier([1, 2, 840, 113549, 1]) + asn1_printablestring("hello.") + asn1_utctime("150223010900Z")), 0)

open(sys.argv[1], 'w').write(asn1)
"""print str(hex(bytestring_to_int(asn1_len(""))))
print str(hex(bytestring_to_int(asn1_len("1"))))
print str(hex(bytestring_to_int(asn1_len("   126"*21))))
print str(hex(bytestring_to_int(asn1_len(" 128"*32))))
print str(hex(bytestring_to_int(asn1_len("     65540"*6554))))

print hex(bytestring_to_int(asn1_boolean(True)))
print hex(bytestring_to_int(asn1_boolean(False)))

print hex(bytestring_to_int(asn1_null()))

print hex(bytestring_to_int(asn1_integer(0)))
print hex(bytestring_to_int(asn1_integer(1)))
print hex(bytestring_to_int(asn1_integer(127)))
print hex(bytestring_to_int(asn1_integer(128)))
print hex(bytestring_to_int(asn1_integer(255)))
print hex(bytestring_to_int(asn1_integer(256)))
print hex(bytestring_to_int(asn1_integer(32767)))
print hex(bytestring_to_int(asn1_integer(32768)))

print hex(bytestring_to_int(asn1_bitstring("")))
print hex(bytestring_to_int(asn1_bitstring("0")))
print hex(bytestring_to_int(asn1_bitstring("1")))
print hex(bytestring_to_int(asn1_bitstring("101010")))
print hex(bytestring_to_int(asn1_bitstring("0011111111")))
print hex(bytestring_to_int(asn1_bitstring("0011111111000000")))

print hex(bytestring_to_int(asn1_octetstring("\x00hohoho")))

print hex(bytestring_to_int(asn1_objectidentifier([1,2])))
print hex(bytestring_to_int(asn1_objectidentifier([1,2,840])))
print hex(bytestring_to_int(asn1_objectidentifier([1,2,840,5,1000000])))
print hex(bytestring_to_int(asn1_objectidentifier([1,2,840,5,127,128,129])))

print hex(bytestring_to_int(asn1_sequence(asn1_null())))

print hex(bytestring_to_int(asn1_set(asn1_null())))

print hex(bytestring_to_int(asn1_printablestring("foo")))

print hex(bytestring_to_int(asn1_utctime("120929010100Z")))

print hex(bytestring_to_int(asn1_tag_explicit(asn1_null(), 0)))
print hex(bytestring_to_int(asn1_tag_explicit(asn1_null(), 4)))
print hex(bytestring_to_int(asn1_tag_explicit(asn1_null(), 30)))"""